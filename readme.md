# Laporan Resmi Soal Shift Sistem Operasi Modul 4 F05 2022

Kelompok F05 :

                - Azzura Mahendra Putra Malinus     5025201211
                - Syaiful Bahri Dirgantara          5025201203
                - Wahyu Tri Saputro                 5025201217

## Pengimplementasian FUSE

Sebelum mengerjakan soal, akan diimplementasikan template FUSE yang akan digunakan untuk mengerjakan soal shift.

### 1. Menginstall FUSE

```bash
$ sudo apt update
$ sudo apt install libfuse*
```

### 2. Menuliskan template FUSE dalam C

Diperlukan sebuah deklarasi macro untuk mendefinisikan versi FUSE yang akan digunakan dan beberapa impor library yang diperlukan, salah satu yang terpenting adalah library `fuse.h`. Selain itu, membuat fungsi-fungsi dasar yang akan digunakan pada FUSE dan dimasukkan ke dalam struct `xmp_oper`. Kemudian, langkah terakhir adalah mengembalikan nilai dari fungsi `fuse_main(argc, argv, &xmp_oper, NULL)` pada main().

```C
#define FUSE_USE_VERSION 28

#include <stdio.h>
#include <fuse.h>
#include <sys/time.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>

static int modified_getattr(const char* path, struct stat* statbuff) {
	int result = lstat(path, statbuff);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_readdir(const char* path, void* buff, fuse_fill_dir_t fill, off_t off, struct fuse_file_info* file_info) {
	(void) off;
	(void) file_info;
	DIR* dir_p;
	struct dirent* dir_ent;

	dir_p = opendir(path);

	if (dir_p == NULL)
		return -errno;

	while ((dir_ent = readdir(dir_p)) != NULL) {
		struct stat stt;
		memset(&stt, 0, sizeof(stt));

		stt.st_ino = dir_ent->d_ino;
		stt.st_mode = dir_ent->d_type << 12;

		if (fill(buff, dir_ent->d_name, &stt, 0))
			break;
	}

	closedir(dir_p);
	return 0;
}

static int modified_read(const char* path, char* buff, size_t sz, off_t off, struct fuse_file_info* file_info) {
	(void) file_info;
	int result, file_dir;

	file_dir = open(path, O_RDONLY);

	if (file_dir < 0)
		return -errno;

	result = pread(file_dir, buff, sz, off);
	close(file_dir);

	if (result < 0)
		return -errno;
	else
		return result;
}

static int modified_write(const char* path, const char* buff, size_t sz, off_t off, struct fuse_file_info* file_info) {
	(void) file_info;
	int result;
	int fd;

	fd = open(path, O_WRONLY);

	if (fd < 0)
		return -errno;

	result = pwrite(fd, buff, sz, off);

	if (result < 0)
		result = -errno;

	close(fd);

	return result;
}

static int modified_mkdir(const char* path, mode_t md) {
	int result = mkdir(path, md);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_rename(const char* old, const char* new) {
	int result = rename(old, new);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_rmdir(const char* path) {
	int result = rmdir(path);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_mknod(const char* path, mode_t md, dev_t rdv) {
	int result;

	if (S_ISREG(md)) {
		result = open(path, O_CREAT | O_EXCL | O_WRONLY, md);

		if (result >= 0)
			result = close(result);
	} else if (S_ISFIFO(md))
		result = mkfifo(path, md);
	else
		result = mknod(path, md, rdv);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_unlink(const char* path) {
	int result = unlink(path);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_open(const char* path, struct fuse_file_info* file_info) {
	int result = open(path, file_info->flags);

	if (result < 0)
		return -errno;
	else {
		close(result);
		return 0;
	}
}

static int modified_readlink(const char* path, char* buff, size_t sz) {
	int result = readlink(path, buff, sz - 1);

	if (result < 0)
		return -errno;

	buff[result] = '\0';

	return 0;
}

static int modified_truncate(const char* path, off_t sz) {
	int result = truncate(path, sz);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static struct fuse_operations xmp_oper = {.read = modified_read, .getattr = modified_getattr, .readdir = modified_readdir,
	.mkdir = modified_mkdir, .rename = modified_rename, .rmdir = modified_rmdir,
	.mknod = modified_mknod, .unlink = modified_unlink, .open = modified_open,
	.write = modified_write, .readlink = modified_readlink, .truncate = modified_truncate,};

int main(int argc, char* argv[]) {
	umask(0);

	return fuse_main(argc, argv, &xmp_oper, NULL);
}

```

### 3. Memodifikasi setiap fungsi pada program sesuai kebutuhan

Setelah template selesai dibuat, selanjutnya adalah memodifikasi setiap fungsi yang telah dibuat sesuai ketentuan soal shift. Fungsi yang dimodifikasi akan dijelaskan pada bagian selanjutnya.

### 4. Meng-compile dan menjalankan program

Untuk meng-compile program, digunakan perintah berikut pada bash:
```bash
$ gcc -Wall `pkg-config fuse --cflags` anya_F05.c -o anya_F05 `pkg-config fuse --libs`
```

Untuk menjalankan program, diguakan perintah berikut pada bash:
```bash
$ ./anya_F05 [Direktori untuk root FUSE]
```

### 5. Membuka FUSE

Setelah program dijalankan, FUSE dapat dibuka pada direktori yang dispesifikkan sebelumnya saat menjalankan program.

## Penjelasan solusi soal shift

## Soal 1

Soal meminta agar file system mengenkripsi nama file dan subdirektori dari isi direktori yang memiliki nama `Animeku_` secara rekursif dengan ketentuan menggunakan encoding atbash cipher untuk huruf besar dan rot13 untuk huruf kecil. Jika `Animeku_` dihilangkan dari nama direktori, nama dari file dan subdirektori dari direktori tersebut akan kembali seperti semula (ter-decode). Karakter '/' dan ektensi tidak perlu dienkripsi.

Semua aktivitas encode dan decode akan tercatat pada sebuah file log `Wibu.log`. Selain itu, soal meminta untuk me-mount FUSE pada directory /home/[USER]/Documents. 

## Solusi

### Membuat variabel global dirpath tempat mount

Variabel ini akan digunakan untuk keperluan logging.

```C
static const char dirpath[] = "/home/azzura13/Documents";
```

### Memodifikasi fungsi modified_rename

Membuat percabangan untuk menangani direktori yang di-rename menjadi Animeku_ dan direktori yang di-rename menjadi semula. Setelah itu, membuat fungsi encode_dir_file untuk melakukan encode dan fungsi decode_dir_file untuk melakukan decode. Kemudian, membuat file `Wibu.log` untuk logging.

```C
static int modified_rename(const char* old, const char* new) {
	int result = rename(old, new);

	char new_str[1000];
	char old_str[1000];

	strcpy(old_str, old);
	strcpy(new_str, new);

	char abs_old[1000];
	char abs_new[1000];

	strcpy(abs_old, dirpath);
	strcat(abs_old, old_str);

	strcpy(abs_new, dirpath);
	strcat(abs_new, new_str);

	// Nomor 1

	// Me-rename direktori menjadi Animeku_
	if (strstr(old, ANIMEKU) == NULL && strstr(new, ANIMEKU) != NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terenkripsi		%s		-->		%s\n", abs_old, abs_new);
		encode_dir_file(new_str, 1, 1);
		fclose(fp);

	// Me-rename direktori menjadi semula
	} else if (strstr(old, ANIMEKU) != NULL && strstr(new, ANIMEKU) == NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terdecode		%s		-->		%s\n", abs_old, abs_new);
		decode_dir_file(new_str, 1, 0);
		fclose(fp);
	}

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

### Membuat fungsi encode_dir_file

Fungsi ini akan meng-encode setiap file dan subdirektori yang ada di dalam direktori yang direname menjadi Animeku_ secara rekursif. Selain itu, kegiatan encode juga akan tercatat pada file `Wibu.log`.

```C
static void encode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				encode_dir_file(new_name, mode, is_encode);

			}
		}
	}

	closedir(dir);
}
```

### Membuat fungsi decode_dir_file

Fungsi ini akan men-decode setiap file dan subdirektori yang ada di dalam direktori yang direname menjadi semula, dengan nama sebelumnya adalah Animeku_, secara rekursif. Selain itu, kegiatan decode juga akan tercatat pada file `Wibu.log`.

```C
static void decode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				decode_dir_file(new_name, mode, is_encode);

			}
		}
	}

	closedir(dir);
}
```

### Membuat fungsi encode

Fungsi ini akan meng-encode/ men-decode sebuah nama yang diberikan kepadanya. Untuk encode/ decode soal 1, dibuat percabangan khusus untuk soal 1. Setiap karakter dari nama akan di-encode/ di-decode sesuai dengan ketentuan.

```C
static void encode(char path[], int mode, int is_encode) {
	int i = 0, j = 0, c;
	int len = strlen(path);

	for (i = 0; i < len; ++i) {
		c = path[i];

		if (mode == 1) {
			if (c == '.')
				break;

			if (c >= 'A' && c <= 'Z') {
				// atbash_chiper(&path[i]);

				c = 'Z' - (c - 'A');
			} else if (c >= 'a' && c <= 'z') {
				// rot13(&path[i]);

				c += 13;

				if (c > 'z')
					c -= 26;
			}
		}

		path[i] = c;
	}
}
```

### Menguji file system

Jika semua selesai diimplementasikan, sebuah direktori pada FUSE akan ter-encode isinya jika namanya diubah menjadi Animeku_ dan akan ter-decode isinya jika namanya dikembalikan. Selain itu, kegiatan encode dan decode juga akan tersimpan pada file `Wibu.log`.

## Soal 2

Soal meminta untuk menambahkan jenis encoding baru jika sebuah direktori diberi nama IAN_. Encoding yang digunakan adalah Vigenere Chiper dengan key `INNUGANTENG`. Selain itu, akan dibuat juga sebuah file log bernama `hayolongapain_F05.log` yang akan dibuat di home directory FUSE. Log ini akan mencatat semua system call yang dipanggil pada FUSE dengan membaginya menjadi dua level, WARNING (untuk unlink dan rmdir) dan INFO (untuk yang lain).

## Solusi

### Memodifikasi fungsi modified_rename

Membuat percabangan untuk menangani direktori yang di-rename menjadi IAN_ dan direktori yang di-rename menjadi semula. Di dalam percabangan, dipanggil fungsi encode_dir_file dan juga decode_dir_file, yang identik dengan percabangan untuk direktori Animeku_. Selain itu, membuat dan memanggil fungsi create_log_IAN untuk membuat log berdasarkan ketentuan soal 2.

```C
static int modified_rename(const char* old, const char* new) {
	int result = rename(old, new);

	char new_str[1000];
	char old_str[1000];

	strcpy(old_str, old);
	strcpy(new_str, new);

	char abs_old[1000];
	char abs_new[1000];

	strcpy(abs_old, dirpath);
	strcat(abs_old, old_str);

	strcpy(abs_new, dirpath);
	strcat(abs_new, new_str);

	// Nomor 1
	if (strstr(old, ANIMEKU) == NULL && strstr(new, ANIMEKU) != NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terenkripsi		%s		-->		%s\n", abs_old, abs_new);
		encode_dir_file(new_str, 1, 1);
		fclose(fp);
	} else if (strstr(old, ANIMEKU) != NULL && strstr(new, ANIMEKU) == NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terdecode		%s		-->		%s\n", abs_old, abs_new);
		decode_dir_file(new_str, 1, 0);
		fclose(fp);
	}

	// Nomor 2
	else if (strstr(old, IAN) == NULL && strstr(new, IAN) != NULL)
		encode_dir_file(new_str, 2, 1);
	else if (strstr(old, IAN) != NULL && strstr(new, IAN) == NULL)
		decode_dir_file(new_str, 2, 0);

	create_log_IAN("INFO", "RENAME", old_str, new_str);

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

### Membuat fungsi create_log_IAN

Fungsi ini akan mencatat berbagai system call yang dipanggil pada FUSE sesuai dengan format yang dijelaskan pada soal 2. Fungsi ini akan dipanggil di beberapa fungsi FUSE yang lain agar aktivitas fungsi tersebut tercatat.

```C
static void create_log_IAN(const char level[], const char scall[], const char parameter1[], const char parameter2[]) {
	FILE *fp = fopen("/home/azzura13/hayolongapain_F05.log", "a+");
	
	time_t current;
    struct tm *curr_time;
    int day, month, year, hour, minute, second;
    char dayStr[3], monthStr[3], yearStr[5], hourStr[3], minStr[3], secStr[3];

    current = time(NULL);
    curr_time = localtime(&current);
	day = curr_time->tm_mday;
	month = curr_time->tm_mon;
	year = curr_time->tm_year;
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
    second = curr_time->tm_sec;

	sprintf(dayStr, "%d", day);
	sprintf(monthStr, "%d", month);
	sprintf(yearStr, "%d", year);
    sprintf(hourStr, "%d", hour);
    sprintf(minStr, "%d", minute);
    sprintf(secStr, "%d", second);

	if (strlen(dayStr) == 1) {
		char temp[3];
        strcpy(temp, "0");
        strcat(temp, dayStr);
        strcpy(dayStr, temp);
	}
	if (strlen(monthStr) == 1) {
		char temp[3];
        strcpy(temp, "0");
        strcat(temp, monthStr);
        strcpy(monthStr, temp);
	} 
	if (strlen(hourStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, hourStr);
        strcpy(hourStr, temp);
    }
    if (strlen(minStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, minStr);
        strcpy(minStr, temp);
    }
    if (strlen(secStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, secStr);
        strcpy(secStr, temp);
    }

	if (strcmp(scall, "RENAME") == 0)
		fprintf(fp, "%s::%s%s%s-%s:%s:%s::%s::%s::%s\n", level, dayStr, monthStr, yearStr, hourStr, minStr, secStr, scall, parameter1, parameter2);
	else
		fprintf(fp, "%s::%s%s%s-%s:%s:%s::%s::%s\n", level, dayStr, monthStr, yearStr, hourStr, minStr, secStr, scall, parameter1);

	fclose(fp);
}
```

Selain dipanggil pada modified_rename, fungsi ini dipanggil saat melakukan beberapa system call berikut:

```C
static int modified_mkdir(const char* path, mode_t md) {
	int result = mkdir(path, md);

	// Nomor 2
	create_log_IAN("INFO", "MKDIR", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

```C
static int modified_rmdir(const char* path) {
	int result = rmdir(path);

	// Nomor 2
	create_log_IAN("WARNING", "RMDIR", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

```C
static int modified_mknod(const char* path, mode_t md, dev_t rdv) {
	int result;

	if (S_ISREG(md)) {
		result = open(path, O_CREAT | O_EXCL | O_WRONLY, md);

		if (result >= 0)
			result = close(result);
	} else if (S_ISFIFO(md))
		result = mkfifo(path, md);
	else
		result = mknod(path, md, rdv);

	create_log_IAN("INFO", "MKNOD", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

```C
static int modified_unlink(const char* path) {
	int result = unlink(path);

	// Nomor 2
	create_log_IAN("WARNING", "UNLINK", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

### Memodifikasi fungsi encode_dir_file

Menambahkan mode == 2 di sebelah mode == 1 (Encoding Animeku_). Pada fungsi ini, soal 2 akan bekerja mirip dengan soal 1, dengan perbedaan pada baris kode yang dijalankan pada fungsi encode dan keterangan yang ditulis pada file Wibu.log.

```C
static void encode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				encode_dir_file(new_name, mode, is_encode);

			}
		}
	}

	closedir(dir);
}
```

### Memodifikasi fungsi decode_dir_file

Menambahkan mode == 2 di sebelah mode == 1 (Encoding Animeku_). Pada fungsi ini, soal 2 akan bekerja mirip dengan soal 1, dengan perbedaan pada baris kode yang dijalankan pada fungsi encode dan keterangan yang ditulis pada file Wibu.log.

```C
static void decode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				decode_dir_file(new_name, mode, is_encode);

			}
		}
	}

	closedir(dir);
}
```

### Memodifikasi fungsi encode

Untuk encode/ decode soal 2, dibuat percabangan khusus untuk soal 2. Setiap karakter dari nama akan di-encode/ di-decode sesuai dengan ketentuan.

```C
static void encode(char path[], int mode, int is_encode) {
	int i = 0, j = 0, c;
	int len = strlen(path);
	
	// Membuat vigenere chiper keystream (Uppercase)
	char key[] = VIGENERE_KEY;
	char keystream[1000];
	for (i = 0; i < len; ++i) {
		if (j == strlen(key))
			j = 0;

		keystream[i] = key[j];
		++j;
	}

	keystream[i] = '\0';
	j = 0;

	for (i = 0; i < len; ++i) {
		c = path[i];

		if (mode == 1) {
			if (c == '.')
				break;

			if (c >= 'A' && c <= 'Z') {
				// atbash_chiper(&path[i]);

				c = 'Z' - (c - 'A');
			} else if (c >= 'a' && c <= 'z') {
				// rot13(&path[i]);

				c += 13;

				if (c > 'z')
					c -= 26;
			}
		} else if (mode == 2) {
			if (is_encode) {
				if (c >= 'A' && c <= 'Z') {
					c = ((c + keystream[j]) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = ((c + keystream[j]) % 26) + 'A';
					c += 32;
					++j;
				}	
			} else {
				if (c >= 'A' && c <= 'Z') {
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					c += 32;
					++j;
				}
			}
		}

		path[i] = c;
	}
}
```

### Menguji file system

Jika semua selesai diimplementasikan, sebuah direktori pada FUSE akan ter-encode isinya jika namanya diubah menjadi IAN_ dan akan ter-decode isinya jika namanya dikembalikan. Selain itu, kegiatan encode dan decode juga akan tersimpan pada file `Wibu.log` dan beberapa system call yang dipanggil akan tercatat pada `hayolongapain.log`.

## Soal 3

Soal meminta untuk menambahkan jenis encoding baru jika sebuah direktori diberi nama nam_do-saq. Encoding hanya akan memengaruhi file, mengubah nama menjadi uppercase, dan mengubah ekstensi desimal perbedaan nama lama dan baru. Desimal diperoleh dari biner perbedaan nama lama dan baru. Sebagai contoh, sISoP dan SISOP memiliki perbedaan biner 10010.

## Solusi

### Memodifikasi fungsi modified_rename

Membuat percabangan untuk menangani direktori yang di-rename menjadi nam_do-saq dan direktori yang di-rename menjadi semula. Di dalam percabangan, dipanggil fungsi encode_dir_file dan juga decode_dir_file, yang identik dengan percabangan untuk direktori Animeku_ dan IAN_.

```C
static int modified_rename(const char* old, const char* new) {
	int result = rename(old, new);

	char new_str[1000];
	char old_str[1000];

	strcpy(old_str, old);
	strcpy(new_str, new);

	char abs_old[1000];
	char abs_new[1000];

	strcpy(abs_old, dirpath);
	strcat(abs_old, old_str);

	strcpy(abs_new, dirpath);
	strcat(abs_new, new_str);

	// Nomor 1
	if (strstr(old, ANIMEKU) == NULL && strstr(new, ANIMEKU) != NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terenkripsi		%s		-->		%s\n", abs_old, abs_new);
		encode_dir_file(new_str, 1, 1);
		fclose(fp);
	} else if (strstr(old, ANIMEKU) != NULL && strstr(new, ANIMEKU) == NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terdecode		%s		-->		%s\n", abs_old, abs_new);
		decode_dir_file(new_str, 1, 0);
		fclose(fp);
	}

	// Nomor 2
	else if (strstr(old, IAN) == NULL && strstr(new, IAN) != NULL)
		encode_dir_file(new_str, 2, 1);
	else if (strstr(old, IAN) != NULL && strstr(new, IAN) == NULL)
		decode_dir_file(new_str, 2, 0);
	
	// Nomor 3
	else if (strstr(old, NAM) == NULL && strstr(new, NAM) != NULL)
		encode_dir_file(new_str, 3, 1);
	else if (strstr(old, NAM) != NULL && strstr(new, NAM) == NULL)
		decode_dir_file(new_str, 3, 0);

	create_log_IAN("INFO", "RENAME", old_str, new_str);

	if (result < 0)
		return -errno;
	else
		return 0;
}
```

### Memodifikasi fungsi encode_dir_file

Menambahkan percabangan untuk mode 3. Di dalam percabangan tersebut, setiap direktori yang ditelusuri akan dicek. Jika memiliki nama Animeku_ atau IAN_, isinya akan di-decode, tanpa men-decode subdirektorinya. Kemudian, jika ditemukan file, akan di-encode dengan encoding soal 3.

```C
static void encode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				encode_dir_file(new_name, mode, is_encode);

			} else if (mode == 3) {
				// Mengembalikan encoding dari ANIMEKU_ dan IAN_ (tidak termasuk subdirektori)

				if (dir_p->d_type == DT_DIR) {
					char temp[1000];

					strcpy(temp, path);
					strcat(temp, "/");
					strcat(temp, dir_p->d_name);

					struct dirent *dir_p_nam;
					DIR* dir_nam = opendir(temp);

					if (!dir_nam) return;

					if (strstr(dir_p->d_name, ANIMEKU) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 1, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}

					} else if (strstr(dir_p->d_name, IAN) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 2, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}
					}
				}

				// Melakukan encoding nam_do-saq
				
				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				if (dir_p->d_type == DT_REG) {
					strcpy(encoded_name, dir_p->d_name);
					encode(encoded_name, mode, is_encode);
					
					// new
					strcpy(new_name, path);
					strcat(new_name, "/");
					strcat(new_name, encoded_name);

					// Wibu.log
					strcpy(abs_new, dirpath);
					strcat(abs_new, new_name);

					rename(old_name, new_name);

					FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
					fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
					fclose(fp);

					create_log_IAN("INFO", "RENAME", old_name, new_name);
					
				} else if (dir_p->d_type == DT_DIR)
					strcpy(new_name, old_name);

				encode_dir_file(new_name, mode, is_encode);
			}
		}
	}

	closedir(dir);
}
```

### Memodifikasi fungsi decode_dir_file

Menambahkan percabangan untuk mode 3. Di dalam percabangan tersebut, setiap direktori yang ditelusuri akan dicek. Jika memiliki nama Animeku_ atau IAN_, isinya akan di-encode, tanpa meng-encode subdirektorinya. Kemudian, jika ditemukan file, akan di-decode dengan encoding soal 3.

```C
static void decode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				decode_dir_file(new_name, mode, is_encode);

			} else if (mode == 3) {
				// Melakukan encoding nam_do-saq
				
				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				if (dir_p->d_type == DT_REG) {
					strcpy(encoded_name, dir_p->d_name);
					encode(encoded_name, mode, is_encode);
					
					// new
					strcpy(new_name, path);
					strcat(new_name, "/");
					strcat(new_name, encoded_name);

					// Wibu.log
					strcpy(abs_new, dirpath);
					strcat(abs_new, new_name);

					rename(old_name, new_name);

					FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
					fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
					fclose(fp);

					create_log_IAN("INFO", "RENAME", old_name, new_name);
					
				} else if (dir_p->d_type == DT_DIR)
					strcpy(new_name, old_name);

				decode_dir_file(new_name, mode, is_encode);

				// Mengembalikan encoding dari ANIMEKU_ dan IAN_ (tidak termasuk subdirektori)

				if (dir_p->d_type == DT_DIR) {
					char temp[1000];

					strcpy(temp, path);
					strcat(temp, "/");
					strcat(temp, dir_p->d_name);

					struct dirent *dir_p_nam;
					DIR* dir_nam = opendir(temp);

					if (!dir_nam) return;

					if (strstr(dir_p->d_name, ANIMEKU) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 1, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}

					} else if (strstr(dir_p->d_name, IAN) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 2, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}
					}
				}
			}
		}
	}

	closedir(dir);
}
```

### Memodifikasi fungsi encode

Untuk encode/ decode soal 3, dibuat percabangan khusus untuk soal 3. Setiap karakter dari nama akan di-encode/ di-decode sesuai dengan ketentuan.

```C
static void encode(char path[], int mode, int is_encode) {
	int i = 0, j = 0, c;
	int len = strlen(path);
	char diff[len];
	unsigned long long diff_int;
	memset(diff, '0', len);
	
	// Membuat vigenere chiper keystream (Uppercase)
	char key[] = VIGENERE_KEY;
	char keystream[1000];
	for (i = 0; i < len; ++i) {
		if (j == strlen(key))
			j = 0;

		keystream[i] = key[j];
		++j;
	}

	keystream[i] = '\0';
	j = 0;

	// (Untuk soal 3) Mendapatkan biner dari perbedaan nama
	if (mode == 3 && !is_encode) {
		while ((c = path[len - j - 1]) != '.') {
			diff[j] = c;
			++j;
		}

		path[len- j- 1] = '\0';
		diff[j] = '\0';
		rev_str(diff);

		char* pEnd;
		diff_int = strtoll(diff, &pEnd, 10);
		llu_to_binstr(diff_int, diff);

	}

	j = 0;

	for (i = 0; i < len; ++i) {
		c = path[i];

		if (mode == 1) {
			if (c == '.')
				break;

			if (c >= 'A' && c <= 'Z') {
				// atbash_chiper(&path[i]);

				c = 'Z' - (c - 'A');
			} else if (c >= 'a' && c <= 'z') {
				// rot13(&path[i]);

				c += 13;

				if (c > 'z')
					c -= 26;
			}
		} else if (mode == 2) {
			if (is_encode) {
				if (c >= 'A' && c <= 'Z') {
					c = ((c + keystream[j]) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = ((c + keystream[j]) % 26) + 'A';
					c += 32;
					++j;
				}	
			} else {
				if (c >= 'A' && c <= 'Z') {
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					c += 32;
					++j;
				}
			}
		} else if (mode == 3) {
			if (is_encode) {
				if (c >= 'a' && c <= 'z') {
					c -= 32;
					diff[i] = '1';
				} else if (c == '.') {
					diff[i] = '\0';
					
					// Mengonversi bin ke long long
					char* pEnd;
					diff_int = strtoll(diff, &pEnd, 2);
					break;
				}
			} else {
				if (diff[i] == '1') {
					if (c >= 'A' && c <= 'Z')
						c += 32;
					else if (c >= 'a' && c <= 'z')
						c -= 32;
					else if (c == '.')
						break;
				}
			}
		}

		path[i] = c;
	}

	if (mode == 3 && is_encode) {
		char new_ext[50];

		sprintf(new_ext, ".%llu", diff_int);
		strcat(path, new_ext);
	}
}
```

### Membuat fungsi tambahan
### Fungsi reverse string
```C
static void rev_str(char str[]) {
	int i, len = strlen(str), temp;

	for (i = 0; i < len/2; ++i) {
		temp = str[i];
		str[i] = str[len-i-1];
		str[len-i-1] = temp;
	}
}
```
### Fungsi konversi unsigned long long ke string biner
```C
static void llu_to_binstr(unsigned long long num, char str[]) {
	int temp, i = 0;

	while (num > 0) {
		temp = num % 2;

		if (temp == 1)
			str[i++] = '1';
		else if (temp == 0)
			str[i++] = '0';

		num = num / 2;
	}

	str[i] = '\0';
	rev_str(str);
}
```

### Menguji file system

Jika semua selesai diimplementasikan, sebuah direktori pada FUSE akan ter-encode isinya jika namanya diubah menjadi nam_do-saq_ dan akan ter-decode isinya jika namanya dikembalikan. Isi dari direktori Animeku_ dan IAN_ akan ter-encode/ ter-decode, berkebalikan dengan encoding nam_do-saq. Selain itu, kegiatan encode dan decode juga akan tersimpan pada file `Wibu.log` dan beberapa system call yang dipanggil akan tercatat pada `hayolongapain.log`.

## Kendala dalam pengerjaan

Dalam pengerjaan, pada awalnya ditemukan kendala dalam memahami cara kerja FUSE dan cara memodifikasi fungsi-fungsi FUSE. Namun, pada akhirnya, kami memahami cara kerjanya dan dapat menggunakannya untuk menyelesaikan soal shift modul 4 ini.

## Dokumentasi

### 1. FUSE pada direktori /home/USER/Documents
![dokum_1](Dokumentasi/1.png)

### 2. Encoding Animeku_
![dokum_2](Dokumentasi/2.png)

### 3. Encoding IAN_
![dokum_3](Dokumentasi/3.png)

### 4. Encoding nam_do-saq_
![dokum_4](Dokumentasi/4.png)

### 5. File Wibu.log
![dokum_5](Dokumentasi/5.png)

### 6. File hayolongapain.log
![dokum_6](Dokumentasi/6.png)

