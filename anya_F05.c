#define FUSE_USE_VERSION 28
#define ANIMEKU "Animeku_"
#define IAN "IAN_"
#define NAM "nam_do-saq_"
#define VIGENERE_KEY "INNUGANTENG"

#include <stdio.h>
#include <fuse.h>
#include <sys/time.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <stdlib.h>

static const char dirpath[] = "/home/azzura13/Documents";

static void rev_str(char str[]) {
	int i, len = strlen(str), temp;

	for (i = 0; i < len/2; ++i) {
		temp = str[i];
		str[i] = str[len-i-1];
		str[len-i-1] = temp;
	}
}

static void llu_to_binstr(unsigned long long num, char str[]) {
	int temp, i = 0;

	while (num > 0) {
		temp = num % 2;

		if (temp == 1)
			str[i++] = '1';
		else if (temp == 0)
			str[i++] = '0';

		num = num / 2;
	}

	str[i] = '\0';
	rev_str(str);
}

static void create_log_IAN(const char level[], const char scall[], const char parameter1[], const char parameter2[]) {
	FILE *fp = fopen("/home/azzura13/hayolongapain_F05.log", "a+");
	
	time_t current;
    struct tm *curr_time;
    int day, month, year, hour, minute, second;
    char dayStr[3], monthStr[3], yearStr[5], hourStr[3], minStr[3], secStr[3];

    current = time(NULL);
    curr_time = localtime(&current);
	day = curr_time->tm_mday;
	month = curr_time->tm_mon;
	year = curr_time->tm_year;
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
    second = curr_time->tm_sec;

	sprintf(dayStr, "%d", day);
	sprintf(monthStr, "%d", month);
	sprintf(yearStr, "%d", year);
    sprintf(hourStr, "%d", hour);
    sprintf(minStr, "%d", minute);
    sprintf(secStr, "%d", second);

	if (strlen(dayStr) == 1) {
		char temp[3];
        strcpy(temp, "0");
        strcat(temp, dayStr);
        strcpy(dayStr, temp);
	}
	if (strlen(monthStr) == 1) {
		char temp[3];
        strcpy(temp, "0");
        strcat(temp, monthStr);
        strcpy(monthStr, temp);
	} 
	if (strlen(hourStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, hourStr);
        strcpy(hourStr, temp);
    }
    if (strlen(minStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, minStr);
        strcpy(minStr, temp);
    }
    if (strlen(secStr) == 1) {
        char temp[3];
        strcpy(temp, "0");
        strcat(temp, secStr);
        strcpy(secStr, temp);
    }

	if (strcmp(scall, "RENAME") == 0)
		fprintf(fp, "%s::%s%s%s-%s:%s:%s::%s::%s::%s\n", level, dayStr, monthStr, yearStr, hourStr, minStr, secStr, scall, parameter1, parameter2);
	else
		fprintf(fp, "%s::%s%s%s-%s:%s:%s::%s::%s\n", level, dayStr, monthStr, yearStr, hourStr, minStr, secStr, scall, parameter1);

	fclose(fp);
}

static void encode(char path[], int mode, int is_encode) {
	int i = 0, j = 0, c;
	int len = strlen(path);
	char diff[len];
	unsigned long long diff_int;
	memset(diff, '0', len);
	
	// Membuat vigenere chiper keystream (Uppercase)
	char key[] = VIGENERE_KEY;
	char keystream[1000];
	for (i = 0; i < len; ++i) {
		if (j == strlen(key))
			j = 0;

		keystream[i] = key[j];
		++j;
	}

	keystream[i] = '\0';
	j = 0;

	// (Untuk soal 3) Mendapatkan biner dari perbedaan nama
	if (mode == 3 && !is_encode) {
		while ((c = path[len - j - 1]) != '.') {
			diff[j] = c;
			++j;
		}

		path[len- j- 1] = '\0';
		diff[j] = '\0';
		rev_str(diff);

		char* pEnd;
		diff_int = strtoll(diff, &pEnd, 10);
		llu_to_binstr(diff_int, diff);

	}

	j = 0;

	for (i = 0; i < len; ++i) {
		c = path[i];

		if (mode == 1) {
			if (c == '.')
				break;

			if (c >= 'A' && c <= 'Z') {
				// atbash_chiper(&path[i]);

				c = 'Z' - (c - 'A');
			} else if (c >= 'a' && c <= 'z') {
				// rot13(&path[i]);

				c += 13;

				if (c > 'z')
					c -= 26;
			}
		} else if (mode == 2) {
			if (is_encode) {
				if (c >= 'A' && c <= 'Z') {
					c = ((c + keystream[j]) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = ((c + keystream[j]) % 26) + 'A';
					c += 32;
					++j;
				}	
			} else {
				if (c >= 'A' && c <= 'Z') {
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					++j;
				} else if (c >= 'a' && c <= 'z') {
					c -= 32;
					c = (((c - keystream[j]) + 26) % 26) + 'A';
					c += 32;
					++j;
				}
			}
		} else if (mode == 3) {
			if (is_encode) {
				if (c >= 'a' && c <= 'z') {
					c -= 32;
					diff[i] = '1';
				} else if (c == '.') {
					diff[i] = '\0';
					
					// Mengonversi bin ke long long
					char* pEnd;
					diff_int = strtoll(diff, &pEnd, 2);
					break;
				}
			} else {
				if (diff[i] == '1') {
					if (c >= 'A' && c <= 'Z')
						c += 32;
					else if (c >= 'a' && c <= 'z')
						c -= 32;
					else if (c == '.')
						break;
				}
			}
		}

		path[i] = c;
	}

	if (mode == 3 && is_encode) {
		char new_ext[50];

		sprintf(new_ext, ".%llu", diff_int);
		strcat(path, new_ext);
	}
}

static void encode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				encode_dir_file(new_name, mode, is_encode);

			} else if (mode == 3) {
				// Mengembalikan encoding dari ANIMEKU_ dan IAN_ (tidak termasuk subdirektori)

				if (dir_p->d_type == DT_DIR) {
					char temp[1000];

					strcpy(temp, path);
					strcat(temp, "/");
					strcat(temp, dir_p->d_name);

					struct dirent *dir_p_nam;
					DIR* dir_nam = opendir(temp);

					if (!dir_nam) return;

					if (strstr(dir_p->d_name, ANIMEKU) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 1, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}

					} else if (strstr(dir_p->d_name, IAN) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 2, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}
					}
				}

				// Melakukan encoding nam_do-saq
				
				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				if (dir_p->d_type == DT_REG) {
					strcpy(encoded_name, dir_p->d_name);
					encode(encoded_name, mode, is_encode);
					
					// new
					strcpy(new_name, path);
					strcat(new_name, "/");
					strcat(new_name, encoded_name);

					// Wibu.log
					strcpy(abs_new, dirpath);
					strcat(abs_new, new_name);

					rename(old_name, new_name);

					FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
					fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
					fclose(fp);

					create_log_IAN("INFO", "RENAME", old_name, new_name);
					
				} else if (dir_p->d_type == DT_DIR)
					strcpy(new_name, old_name);

				encode_dir_file(new_name, mode, is_encode);
			}
		}
	}

	closedir(dir);
}

static void decode_dir_file(char path[], int mode, int is_encode) {
	struct dirent *dir_p;
	DIR* dir = opendir(path);
	char encoded_name[1000];
	char old_name[1000];
	char new_name[1000];
	char abs_old[1000];
	char abs_new[1000];

	if (!dir) return;

	while ((dir_p = readdir(dir)) != NULL) {
		if (strcmp(dir_p->d_name, "..") != 0 && strcmp(dir_p->d_name, ".") != 0) {
			if (mode == 1 || mode == 2) {
				strcpy(encoded_name, dir_p->d_name);
				encode(encoded_name, mode, is_encode);

				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				// new
				strcpy(new_name, path);
				strcat(new_name, "/");
				strcat(new_name, encoded_name);

				// Wibu.log
				strcpy(abs_new, dirpath);
				strcat(abs_new, new_name);

				rename(old_name, new_name);

				FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
				fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
				fclose(fp);

				create_log_IAN("INFO", "RENAME", old_name, new_name);

				decode_dir_file(new_name, mode, is_encode);

			} else if (mode == 3) {
				// Melakukan encoding nam_do-saq
				
				// old
				strcpy(old_name, path);
				strcat(old_name, "/");
				strcat(old_name, dir_p->d_name);

				// Wibu.log
				strcpy(abs_old, dirpath);
				strcat(abs_old, old_name);

				if (dir_p->d_type == DT_REG) {
					strcpy(encoded_name, dir_p->d_name);
					encode(encoded_name, mode, is_encode);
					
					// new
					strcpy(new_name, path);
					strcat(new_name, "/");
					strcat(new_name, encoded_name);

					// Wibu.log
					strcpy(abs_new, dirpath);
					strcat(abs_new, new_name);

					rename(old_name, new_name);

					FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
					fprintf(fp, "RENAME		terdecode		%s 		-->		%s\n", abs_old, abs_new);
					fclose(fp);

					create_log_IAN("INFO", "RENAME", old_name, new_name);
					
				} else if (dir_p->d_type == DT_DIR)
					strcpy(new_name, old_name);

				decode_dir_file(new_name, mode, is_encode);

				// Mengembalikan encoding dari ANIMEKU_ dan IAN_ (tidak termasuk subdirektori)

				if (dir_p->d_type == DT_DIR) {
					char temp[1000];

					strcpy(temp, path);
					strcat(temp, "/");
					strcat(temp, dir_p->d_name);

					struct dirent *dir_p_nam;
					DIR* dir_nam = opendir(temp);

					if (!dir_nam) return;

					if (strstr(dir_p->d_name, ANIMEKU) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 1, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}

					} else if (strstr(dir_p->d_name, IAN) != NULL) {
						while ((dir_p_nam = readdir(dir_nam)) != NULL) {
							if (strcmp(dir_p_nam->d_name, "..") != 0 && strcmp(dir_p_nam->d_name, ".") != 0) {
								strcpy(encoded_name, dir_p_nam->d_name);
								encode(encoded_name, 2, !is_encode);

								// old
								strcpy(old_name, temp);
								strcat(old_name, "/");
								strcat(old_name, dir_p_nam->d_name);

								// Wibu.log
								strcpy(abs_old, dirpath);
								strcat(abs_old, old_name);

								// new
								strcpy(new_name, temp);
								strcat(new_name, "/");
								strcat(new_name, encoded_name);

								// Wibu.log
								strcpy(abs_new, dirpath);
								strcat(abs_new, new_name);

								rename(old_name, new_name);

								FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
								fprintf(fp, "RENAME		terenkripsi		%s 		-->		%s\n", abs_old, abs_new);
								fclose(fp);

								create_log_IAN("INFO", "RENAME", old_name, new_name);
							}
						}
					}
				}
			}
		}
	}

	closedir(dir);
}

static int modified_getattr(const char* path, struct stat* statbuff) {
	int result = lstat(path, statbuff);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_readdir(const char* path, void* buff, fuse_fill_dir_t fill, off_t off, struct fuse_file_info* file_info) {
	(void) off;
	(void) file_info;
	DIR* dir_p;
	struct dirent* dir_ent;

	dir_p = opendir(path);

	if (dir_p == NULL)
		return -errno;

	while ((dir_ent = readdir(dir_p)) != NULL) {
		struct stat stt;
		memset(&stt, 0, sizeof(stt));

		stt.st_ino = dir_ent->d_ino;
		stt.st_mode = dir_ent->d_type << 12;

		if (fill(buff, dir_ent->d_name, &stt, 0))
			break;
	}

	closedir(dir_p);
	return 0;
}

static int modified_read(const char* path, char* buff, size_t sz, off_t off, struct fuse_file_info* file_info) {
	(void) file_info;
	int result, file_dir;

	file_dir = open(path, O_RDONLY);

	if (file_dir < 0)
		return -errno;

	result = pread(file_dir, buff, sz, off);
	close(file_dir);

	if (result < 0)
		return -errno;
	else
		return result;
}

static int modified_write(const char* path, const char* buff, size_t sz, off_t off, struct fuse_file_info* file_info) {
	(void) file_info;
	int result;
	int fd;

	fd = open(path, O_WRONLY);

	if (fd < 0)
		return -errno;

	result = pwrite(fd, buff, sz, off);

	if (result < 0)
		result = -errno;

	close(fd);

	return result;
}

static int modified_mkdir(const char* path, mode_t md) {
	int result = mkdir(path, md);

	// Nomor 2
	create_log_IAN("INFO", "MKDIR", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_rename(const char* old, const char* new) {
	int result = rename(old, new);

	char new_str[1000];
	char old_str[1000];

	strcpy(old_str, old);
	strcpy(new_str, new);

	char abs_old[1000];
	char abs_new[1000];

	strcpy(abs_old, dirpath);
	strcat(abs_old, old_str);

	strcpy(abs_new, dirpath);
	strcat(abs_new, new_str);

	// Nomor 1
	if (strstr(old, ANIMEKU) == NULL && strstr(new, ANIMEKU) != NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terenkripsi		%s		-->		%s\n", abs_old, abs_new);
		encode_dir_file(new_str, 1, 1);
		fclose(fp);
	} else if (strstr(old, ANIMEKU) != NULL && strstr(new, ANIMEKU) == NULL) {
		FILE* fp = fopen("/home/azzura13/SisopModul4/Wibu.log", "a+");
		fprintf(fp, "RENAME		terdecode		%s		-->		%s\n", abs_old, abs_new);
		decode_dir_file(new_str, 1, 0);
		fclose(fp);
	}

	// Nomor 2
	else if (strstr(old, IAN) == NULL && strstr(new, IAN) != NULL)
		encode_dir_file(new_str, 2, 1);
	else if (strstr(old, IAN) != NULL && strstr(new, IAN) == NULL)
		decode_dir_file(new_str, 2, 0);
	
	// Nomor 3
	else if (strstr(old, NAM) == NULL && strstr(new, NAM) != NULL)
		encode_dir_file(new_str, 3, 1);
	else if (strstr(old, NAM) != NULL && strstr(new, NAM) == NULL)
		decode_dir_file(new_str, 3, 0);

	create_log_IAN("INFO", "RENAME", old_str, new_str);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_rmdir(const char* path) {
	int result = rmdir(path);

	// Nomor 2
	create_log_IAN("WARNING", "RMDIR", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_mknod(const char* path, mode_t md, dev_t rdv) {
	int result;

	if (S_ISREG(md)) {
		result = open(path, O_CREAT | O_EXCL | O_WRONLY, md);

		if (result >= 0)
			result = close(result);
	} else if (S_ISFIFO(md))
		result = mkfifo(path, md);
	else
		result = mknod(path, md, rdv);

	create_log_IAN("INFO", "MKNOD", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_unlink(const char* path) {
	int result = unlink(path);

	// Nomor 2
	create_log_IAN("WARNING", "UNLINK", path, "");

	if (result < 0)
		return -errno;
	else
		return 0;
}

static int modified_open(const char* path, struct fuse_file_info* file_info) {
	int result = open(path, file_info->flags);

	// create_log_IAN("INFO", "OPEN", path, "");

	if (result < 0)
		return -errno;
	else {
		close(result);
		return 0;
	}
}

static int modified_readlink(const char* path, char* buff, size_t sz) {
	int result = readlink(path, buff, sz - 1);

	if (result < 0)
		return -errno;

	buff[result] = '\0';

	return 0;
}

static int modified_truncate(const char* path, off_t sz) {
	int result = truncate(path, sz);

	if (result < 0)
		return -errno;
	else
		return 0;
}

static struct fuse_operations xmp_oper = {.read = modified_read, .getattr = modified_getattr, .readdir = modified_readdir,
	.mkdir = modified_mkdir, .rename = modified_rename, .rmdir = modified_rmdir,
	.mknod = modified_mknod, .unlink = modified_unlink, .open = modified_open,
	.write = modified_write, .readlink = modified_readlink, .truncate = modified_truncate,};

int main(int argc, char* argv[]) {
	umask(0);

	return fuse_main(argc, argv, &xmp_oper, NULL);
}
